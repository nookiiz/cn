package com.example.gt72.newcn.service;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.example.gt72.newcn.MainActivity;
import com.example.gt72.newcn.R;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Created by GT72 on 29/11/2559.
 */

public class myService extends Service {
    private Timer mTimer;
    private String id_activities;
    private String name_title;
    private String description;
    private long timestamp;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = intent.getExtras();
        this.id_activities = bundle.getString("id_activities");
        this.name_title = bundle.getString("name_title");
        this.description = bundle.getString("description");
        this.timestamp = bundle.getLong("timestamp");
        /*
        Toast.makeText(this, time, Toast.LENGTH_SHORT).show();

        Calendar today = Calendar.getInstance();
        String[] tmp = time.split(" ");
        String[] tmpdate = tmp[0].split("-");
        String[] tmptime = tmp[1].split(":");

        today.set(Calendar.DAY_OF_MONTH, Integer.parseInt(tmpdate[0]));
        today.set(Calendar.MONTH, Integer.parseInt(tmpdate[1]));
        today.set(Calendar.YEAR, Integer.parseInt(tmpdate[2]));

        today.set(Calendar.HOUR_OF_DAY, Integer.parseInt(tmptime[0]));
        today.set(Calendar.MINUTE, Integer.parseInt(tmptime[1]));
        today.set(Calendar.SECOND, Integer.parseInt(tmptime[2]));
        Timer timer = new Timer();
        mTimer.schedule(timerTask, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS)); // 60*60*24*100 = 8640000ms
        */
        mTimer = new Timer();
        mTimer.schedule(timerTask, new Date((timestamp+10) * 1000));
        //Toast.makeText(this, timestamp + "", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        mTimer.schedule(timerTask, 60000, 60000);
    }

    TimerTask timerTask = new TimerTask() {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void run() {
            Log.e("Log", "Running");
            notifiy();
            onDestroy();
        }
    };

    @Override
    public void onDestroy() {
        try {
            mTimer.cancel();
            timerTask.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent("com.example.gt72.newcn");
        intent.putExtra("yourvalue", "torestore");
        sendBroadcast(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void notifiy() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("RSSPullService");

        // Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        Intent myIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, myIntent, 0);
        Context context = getApplicationContext();

        Notification.Builder builder;
        builder = new Notification.Builder(context)
                .setContentTitle(name_title)
                .setContentText(description)
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.up);
        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);

    }

}
