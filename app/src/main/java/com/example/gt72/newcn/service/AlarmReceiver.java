package com.example.gt72.newcn.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

import com.example.gt72.newcn.MainActivity;
import com.example.gt72.newcn.R;
import com.example.gt72.newcn.even.DataEven;

/**
 * Created by DRAF on 6/12/2559.
 */
public class AlarmReceiver extends BroadcastReceiver {
    DataEven dataEven;
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onReceive(Context context, Intent intent) {
        dataEven =  (DataEven) intent.getSerializableExtra("dataEven");
        Toast.makeText(context, dataEven.getName(), Toast.LENGTH_SHORT).show();
        //Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));
        Intent myIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder;
        builder = new Notification.Builder(context)
                .setContentTitle(dataEven.getName())
                .setContentText(dataEven.getDescription())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);
        Notification notification = builder.build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }


}