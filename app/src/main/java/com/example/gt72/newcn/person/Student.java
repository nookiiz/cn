package com.example.gt72.newcn.person;

/**
 * Created by DRAF on 4/12/2559.
 */

public class Student extends Person {

    private String code;
    private String majoren;
    private String majorth;
    private String facultyen;
    private String facultyth;
    private String email;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMajoren() {
        return majoren;
    }

    public void setMajoren(String majoren) {
        this.majoren = majoren;
    }

    public String getMajorth() {
        return majorth;
    }

    public void setMajorth(String majorth) {
        this.majorth = majorth;
    }

    public String getFacultyen() {
        return facultyen;
    }

    public void setFacultyen(String facultyen) {
        this.facultyen = facultyen;
    }

    public String getFacultyth() {
        return facultyth;
    }

    public void setFacultyth(String facultyth) {
        this.facultyth = facultyth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
