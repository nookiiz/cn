package com.example.gt72.newcn.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.gt72.newcn.service.myService;

/**
 * Created by GT72 on 29/11/2559.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("Service Stop","Ohhhhh");
        context.startService(new Intent(context,myService.class));
    }
}
