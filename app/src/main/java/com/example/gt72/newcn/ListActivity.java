package com.example.gt72.newcn;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.gt72.newcn.even.DataEven;
import com.example.gt72.newcn.libraries.Adapter;
import com.example.gt72.newcn.libraries.Curl;
import com.example.gt72.newcn.person.Student;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by GT72 on 22/11/2559.
 */

public class ListActivity extends AppCompatActivity {
    ArrayList<DataEven> array_dataEven = new ArrayList<DataEven>();
    DataEven dataEven;
    Student student;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        student =  (Student) getIntent().getSerializableExtra("student");
        getFEED();
    }

    private void getFEED() {
        Curl curl = new Curl();
        try {
            curl.ActionGET("http://res.ict.up.ac.th/coop/course/list?offset=0&limit=10000");
            String data = curl.getDATA();
            JSONArray Jarray = new JSONArray(data);

            for (int i = 0; i < Jarray.length(); i++) {
                JSONObject Obj_tmp = Jarray.getJSONObject(i);
                dataEven = new DataEven();
                dataEven.setId(Obj_tmp.getInt("id"));
                dataEven.setName(Obj_tmp.getString("name"));
                dataEven.setDescription(Obj_tmp.getString("detail"));
                dataEven.setLocation(Obj_tmp.getString("place"));

                long time = Long.parseLong(Obj_tmp.getString("start"));
                java.util.Calendar calendar = java.util.Calendar.getInstance();
                java.util.TimeZone tz = java.util.TimeZone.getDefault();
                calendar.add(java.util.Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                java.util.Date currenTimeZone = new java.util.Date((long) time * 1000);

                dataEven.setDatetime(sdf.format(currenTimeZone));
                dataEven.setTimestamp(Obj_tmp.getString("end"));
                dataEven.setImg(R.mipmap.ic_launcher);
                array_dataEven.add(dataEven);
                /*id_activities.add(Obj_tmp.getString("id"));
                name_title.add(Obj_tmp.getString("name"));
                location.add(Obj_tmp.getString("place"));
                description.add(Obj_tmp.getString("detail"));
                timestamp.add(Obj_tmp.getString("start"));

                long time = Long.parseLong(Obj_tmp.getString("start"));
                java.util.Calendar calendar = java.util.Calendar.getInstance();
                java.util.TimeZone tz = java.util.TimeZone.getDefault();
                calendar.add(java.util.Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
                java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                java.util.Date currenTimeZone = new java.util.Date((long) time * 1000);

                datetime.add(sdf.format(currenTimeZone));
                img.add(R.mipmap.ic_launcher);
                */
            }
            ListView ListFEED = (ListView) findViewById(R.id.ListNews);
            //Adapter adapter = new Adapter(this, name_title, location, datetime, img);
            Adapter adapter = new Adapter(this, array_dataEven);
            ListFEED.setAdapter(adapter);
            ListFEED.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    viewContent(position);
                }
            });
        } catch (Exception e) {
            AlertDialog alertDialog = new AlertDialog.Builder(ListActivity.this).create();
            alertDialog.setTitle("แจ้งเตือน");
            alertDialog.setMessage("เกิดปัญหา getFEED กรุณาลองใหม่อีกครั้ง");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    private void viewContent(int position) {
        Intent view = new Intent(this, ViewActivity.class);
        view.putExtra("dataEven", array_dataEven.get(position));
        startActivity(view);
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("type", "student");
        intent.putExtra("student", student);
        startActivity(intent);
    }
}
