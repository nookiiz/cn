package com.example.gt72.newcn;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gt72.newcn.libraries.Curl;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONObject;

public class QRScanActivity extends AppCompatActivity {
    private Button btn_scan, btn_submit;
    private EditText student_id, Event_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscan);
        btn_scan = (Button) findViewById(R.id.btn_scan);
        btn_submit = (Button) findViewById(R.id.btn_submit);

        student_id = (EditText) findViewById(R.id.student_id);
        Event_id = (EditText) findViewById(R.id.Event_id);


        final Activity activity = this;
        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentIntegrator intentIntegrator = new IntentIntegrator(activity);
                intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
                intentIntegrator.setPrompt("Scan");
                intentIntegrator.setCameraId(0);
                intentIntegrator.setBeepEnabled(true);
                intentIntegrator.setBarcodeImageEnabled(false);
                intentIntegrator.initiateScan();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Event_id.getText().toString().trim().equals("") && !student_id.getText().toString().trim().equals("")) {
                    Curl curl = new Curl();
                    curl.ActionGET("http://res.ict.up.ac.th/58022120/UP/api/checkin.php?even_id="+Event_id.getText().toString().trim()+"&student_id="+student_id.getText().toString().trim());
                    String _data = curl.getDATA();
                    if(_data != null){
                        try {
                            JSONObject data = new JSONObject(_data);
                            if(data.getBoolean("status")) {
                                Toast.makeText(QRScanActivity.this, "success", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(QRScanActivity.this, "fail", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(QRScanActivity.this, "Error #1", Toast.LENGTH_SHORT).show();
                        }
                    }else {
                        Toast.makeText(QRScanActivity.this, "Error #2", Toast.LENGTH_SHORT).show();
                    }
                }
                student_id.setText("");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "Cancel", Toast.LENGTH_SHORT).show();
            } else {
                student_id.setText(result.getContents());
                Toast.makeText(this, "result = " + result.getContents(), Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
}
