package com.example.gt72.newcn;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.gt72.newcn.libraries.Login;


public class MainActivity extends AppCompatActivity {
    AlertDialog alertDialog;
    Button btnclick;

    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!isOnline()) return;
        btnclick = (Button) findViewById(R.id.btn_login);
        btnclick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                progress = ProgressDialog.show(MainActivity.this, "Loading", "กรุณารอสักครู่...", true);
                progress.setCancelable(false);
                progress.setCanceledOnTouchOutside(false);

                btnclick.setText("กรุณารอสักครู่...");
                btnclick.setEnabled(false);

                if (!isOnline()) return;
                EditText id = (EditText) findViewById(R.id.id);
                EditText pass = (EditText) findViewById(R.id.pass);
                Login login = new Login(MainActivity.this,progress);
                login.ActionLogin(id, pass);
                btnclick.setBackgroundColor(Color.GRAY);
            }

        });
        password_reset();
    }

    private void password_reset() {

        TextView password_reset = (TextView) findViewById(R.id.password_reset);
        password_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isOnline()) return;
                try {
                    Uri uri = Uri.parse("http://res.ict.up.ac.th/coop/site/request-password-reset");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (Exception e) {
                    Log.e("Run", "Error " + e);
                }
            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("แจ้งเตือน");
        alertDialog.setMessage("ควรทำการเชื่อมต่อกับ network ก่อน");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
        return false;
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}
