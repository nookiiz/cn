package com.example.gt72.newcn;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.example.gt72.newcn.even.DataEven;
import com.example.gt72.newcn.service.AlarmReceiver;
import com.example.gt72.newcn.service.myService;

import java.util.Calendar;

/**
 * Created by DRAF on 31/10/2559.
 */

public class ViewActivity extends AppCompatActivity {

    DataEven dataEven;
    private PendingIntent pendingIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcontent);
        dataEven =  (DataEven) getIntent().getSerializableExtra("dataEven");
        viewSHOW();
    }

    private void viewSHOW() {
        TextView view_title = (TextView) findViewById(R.id.view_title);
        TextView view_desc = (TextView) findViewById(R.id.view_desc);
        TextView view_location = (TextView) findViewById(R.id.view_location);
        final TextView view_time = (TextView) findViewById(R.id.view_time);
        Button btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                long tsLong = System.currentTimeMillis() / 1000;
                Intent alarmIntent = new Intent(ViewActivity.this, AlarmReceiver.class);
                alarmIntent.putExtra("dataEven",dataEven);
                pendingIntent = PendingIntent.getBroadcast(ViewActivity.this, 0, alarmIntent, 0);
                startAt(tsLong+10);
                Toast.makeText(ViewActivity.this, "ตั้งเวลาเรียบร้อยแล้ว", Toast.LENGTH_SHORT).show();
                 /*
                 if ((_timestamp - 1 * 86400) + 61200 > System.currentTimeMillis() / 1000) {
                    Intent intent = new Intent(ViewActivity.this, myService.class);
                    intent.putExtra("id_activities", id_activities);
                    intent.putExtra("name_title", name_title);
                    intent.putExtra("description", description);
                    intent.putExtra("timestamp", (long) ((_timestamp - 1 * 86400) + 61200));
                    startService(intent);
                    Toast.makeText(ViewActivity.this, "1", Toast.LENGTH_SHORT).show();

                }
               if ((_timestamp - 3 * 86400) + 61200 > System.currentTimeMillis() / 1000) {
                    Intent intent = new Intent(ViewActivity.this, myService.class);
                    intent.putExtra("id_activities", id_activities);
                    intent.putExtra("name_title", name_title);
                    intent.putExtra("description", description);
                    intent.putExtra("timestamp", (long) ((_timestamp - 3 * 86400) + 61200));
                    startService(intent);
                    Toast.makeText(ViewActivity.this, "3", Toast.LENGTH_SHORT).show();
                }
                if ((_timestamp - 7 * 86400) + 61200 > System.currentTimeMillis() / 1000) {
                    Intent intent = new Intent(ViewActivity.this, myService.class);
                    intent.putExtra("id_activities", id_activities);
                    intent.putExtra("name_title", name_title);
                    intent.putExtra("description", description);
                    intent.putExtra("timestamp", (long) ((_timestamp - 7 * 86400) + 61200));
                    startService(intent);
                    Toast.makeText(ViewActivity.this, "7", Toast.LENGTH_SHORT).show();
                }
                if ((_timestamp - 1 * 86400) + 61200 <= System.currentTimeMillis() / 1000 && (_timestamp - 3 * 86400) + 61200 <= System.currentTimeMillis() / 1000 && (_timestamp - 7 * 86400) + 61200 <= System.currentTimeMillis() / 1000) {
                    AlertDialog alertDialog = new AlertDialog.Builder(ViewActivity.this).create();
                    alertDialog.setTitle("แจ้งเตือน");
                    alertDialog.setMessage("ไม่สามารถติดตามการอบรมนี้ได้\nเนื่องจากการอบรมนี้ผ่านมาแล้ว");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return;
                }
                */


            }
        });

        view_title.setText(dataEven.getName());
        view_desc.setText(dataEven.getDescription());
        view_location.setText("สถานที่ : " + dataEven.getLocation());
        view_time.setText("เวลาและวันที่ : " + dataEven.getDatetime());
    }

    public void startAt(long getTimeInMillis) {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
       /* Set the alarm to start at 10:30 AM */
       /* Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.DAY_OF_MONTH, 6);
        calendar.set(Calendar.HOUR_OF_DAY, 3);
        calendar.set(Calendar.MINUTE, 10);
        calendar.set(Calendar.SECOND, 0);*/

        /* Repeating on every 20 minutes interval */
        manager.set(AlarmManager.RTC_WAKEUP, getTimeInMillis*1000, pendingIntent);
    }
}
