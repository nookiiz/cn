package com.example.gt72.newcn;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.gt72.newcn.person.Student;

/**
 * Created by GT72 on 29/11/2559.
 */

public class MenuActivity extends AppCompatActivity {

    Student student;
    String type;
    private ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Intent intent = getIntent();
        type = intent.getExtras().getString("type");
        if(type.trim().equals("student")){
            student = (Student) getIntent().getSerializableExtra("student");
        }
        TextView textView = (TextView) findViewById(R.id.textView3);
        textView.setText(student.getFullname());
        progress = new ProgressDialog(this);
        progress.dismiss();

        Button btngetfeed = (Button) findViewById(R.id.btn_getfeed);
        btngetfeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progress.setTitle("Loading");
                progress.setMessage("กรุณารอสักครู่...");
                progress.show();
                Intent intent = new Intent(MenuActivity.this, ListActivity.class);
                intent.putExtra("student", student);
                startActivity(intent);
            }
        });
        Button btn_checkin = (Button) findViewById(R.id.btn_checkin);
        if (!student.getHelper()){
            btn_checkin.setVisibility(View.INVISIBLE);
            LinearLayout layout = (LinearLayout) btn_checkin.getParent();
            if(null!=layout) //for safety only  as you are doing onClick
                layout.removeView(btn_checkin);
        }
        btn_checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, QRScanActivity.class);
                startActivity(intent);
            }
        });

        Button btn_logout = (Button) findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }
    @Override
    public void onBackPressed() {
        finish();
        startActivity(getIntent());
    }
}
