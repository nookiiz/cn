package com.example.gt72.newcn.libraries;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.gt72.newcn.MenuActivity;
import com.example.gt72.newcn.R;
import com.example.gt72.newcn.even.DataEven;
import com.example.gt72.newcn.libraries.Curl;
import com.example.gt72.newcn.person.Student;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

/**
 * Created by GT72 on 22/11/2559.
 */

public class Login {

    private AppCompatActivity MainActivity;
    AlertDialog alertDialog;
    ProgressDialog progress;

    String idbase64;
    String passbase64;
    Student student;
    DataEven dataEven;
    Button btnlogin;
    public Login(AppCompatActivity MainActivity,ProgressDialog progress) {
        this.MainActivity = MainActivity;
        this.progress = progress;
        this.btnlogin = (Button) MainActivity.findViewById(R.id.btn_login);
    }


    public void ActionLogin(EditText user_code, EditText user_pass) {
        if (user_code.getText().length() > 7 && user_pass.getText().length() > 0) {
            Curl curl = new Curl();
            try {
                byte[] byteid = user_code.getText().toString().trim().getBytes("UTF-8");
                byte[] bytepass = user_pass.getText().toString().trim().getBytes("UTF-8");
                idbase64 = Base64.encodeToString(byteid, Base64.NO_WRAP);
                passbase64 = Base64.encodeToString(bytepass, Base64.NO_WRAP);
                String urlLogin = "http://res.ict.up.ac.th/coop/site/api/?type=login&method=" + idbase64.length() + "dkttthai" + idbase64 + "" + passbase64 + "";
                curl.ActionGET(urlLogin);

            } catch (UnsupportedEncodingException e) {
                alertDialog = new AlertDialog.Builder(MainActivity).create();
                alertDialog.setTitle("แจ้งเตือน");
                alertDialog.setMessage("ERROR: 0 !!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                set_Enabled();
                            }
                        });
                alertDialog.show();
            }

            String _data = curl.getDATA();
            if (_data != null) try {
                JSONObject data = new JSONObject(_data);
                if (data.getBoolean("status")) {

                    if (data.getString("privilege").trim().equals("STUDENT")) {
                        student = new Student();
                        student.setUsername(idbase64);
                        student.setPassword(passbase64);
                        student.setCode(data.getString("username").trim());
                        student.setPrefix("คุณ");
                        student.setFirstName(data.getString("name").trim());
                        student.setLastName(data.getString("surname").trim());
                        student.setMajoren(data.getString("majoren").trim());
                        student.setMajorth(data.getString("majorth").trim());
                        student.setFacultyen(data.getString("facultyen").trim());
                        student.setFacultyth(data.getString("facultyth").trim());
                        student.setEmail(data.getString("email").trim());
                        student.setHelper(data.getBoolean("helper"));
                        if (student.getHelper()) {
                            JSONArray Jarray = new JSONArray(data.getString("dataevan").trim());
                            for (int i = 0; i < Jarray.length(); i++) {
                                JSONObject Obj_tmp = Jarray.getJSONObject(i);
                                dataEven = new DataEven();
                                dataEven.setId(Obj_tmp.getInt("id"));
                                dataEven.setName(Obj_tmp.getString("name").trim());
                                dataEven.setTimestamp(Obj_tmp.getString("timestamp"));
                                student.addDataEvan(dataEven);
                            }
                        }
                        //Toast.makeText(MainActivity, student.getFullname(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity, MenuActivity.class);
                        intent.putExtra("type", "student");
                        intent.putExtra("student", student);
                        MainActivity.startActivity(intent);
                    }


                } else {
                    alertDialog = new AlertDialog.Builder(MainActivity).create();
                    alertDialog.setTitle("แจ้งเตือน");
                    alertDialog.setMessage(data.getString("message"));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                    set_Enabled();
                                }
                            });
                    alertDialog.show();
                }
            } catch (Exception e) {
                alertDialog = new AlertDialog.Builder(MainActivity).create();
                alertDialog.setTitle("แจ้งเตือน");
                alertDialog.setMessage("ERROR: 1 !!");
                Toast.makeText(MainActivity, "" + e, Toast.LENGTH_LONG).show();
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                set_Enabled();
                            }
                        });
                alertDialog.show();
            }
            else {
                alertDialog = new AlertDialog.Builder(MainActivity).create();
                alertDialog.setTitle("แจ้งเตือน");
                alertDialog.setMessage("ERROR: 2 !!");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                                set_Enabled();
                            }
                        });
                alertDialog.show();
            }

        } else {
            alertDialog = new AlertDialog.Builder(MainActivity).create();
            alertDialog.setTitle("แจ้งเตือน");
            alertDialog.setMessage("โปรดระบุข้อมูลให้ครบถ้วน !!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            set_Enabled();
                        }
                    });
            alertDialog.show();
        }
    }
    private void set_Enabled(){
        progress.dismiss();
        btnlogin.setEnabled(true);
        btnlogin.setText("SIGN IN");
        btnlogin.setBackgroundColor(Color.parseColor("#f29a16"));
    }
}
