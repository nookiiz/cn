package com.example.gt72.newcn.person;

import com.example.gt72.newcn.even.DataEven;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by DRAF on 4/12/2559.
 */

public class Person implements Serializable {

    private String prefix;
    private String firstName;
    private String lastName;
    private boolean helper;
    private String username;
    private String password;
    private ArrayList<DataEven> dataEvan = new ArrayList<DataEven>();

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean getHelper() {
        return helper;
    }

    public void setHelper(boolean helper) {
        this.helper = helper;
    }

    public String getFullname() {
        return prefix + " " + firstName + " " + lastName;
    }

    public ArrayList<DataEven> getDataEvan() {
        return dataEvan;
    }

    public void addDataEvan(DataEven dataEvan) {
        this.dataEvan.add(dataEvan);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
