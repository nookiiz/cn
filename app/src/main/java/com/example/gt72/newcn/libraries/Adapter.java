package com.example.gt72.newcn.libraries;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gt72.newcn.R;
import com.example.gt72.newcn.even.DataEven;

import java.util.ArrayList;

/**
 * Created by DRAF on 30/10/2559.
 */

public class Adapter extends ArrayAdapter<DataEven> {

    Context context;
    /* ArrayList<String> name_title;
     ArrayList<String> location;
     ArrayList<String> datetime;
     ArrayList<Integer> img;*/
    LayoutInflater inflater;

    ArrayList<DataEven> array_dataEven;

/*    public Adapter(Context context, ArrayList<String> name_title, ArrayList<String> location, ArrayList<String> datetime, ArrayList<Integer> img) {
        super(context, R.layout.item_model, name_title);
        this.context = context;
        this.name_title = name_title;
        this.location = location;
        this.datetime = datetime;
        this.img = img;
    }
*/
    public Adapter(Context context, ArrayList<DataEven> array_dataEven) {
        super(context, R.layout.item_model, array_dataEven);
        this.context = context;
        this.array_dataEven = array_dataEven;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_model, null);
        }

        TextView view_name = (TextView) convertView.findViewById(R.id.view_name);
        TextView view_location = (TextView) convertView.findViewById(R.id.view_location);
        TextView view_datetime = (TextView) convertView.findViewById(R.id.view_datetime);
        ImageView img_time = (ImageView) convertView.findViewById(R.id.img_time);

        /*view_name.setText(name_title.get(position));
        view_location.setText("สถานที่ : " + location.get(position));
        view_datetime.setText("เวลา : " + datetime.get(position));
        img_time.setBackgroundResource(img.get(position));
        //imgTv.setImageResource(img[position]);
        */

        view_name.setText(array_dataEven.get(position).getName());
        view_location.setText("สถานที่ : " + array_dataEven.get(position).getLocation());
        view_datetime.setText("เวลา : " + array_dataEven.get(position).getDatetime());
        img_time.setBackgroundResource(array_dataEven.get(position).getImg());
        //imgTv.setImageResource(img[position]);
        return convertView;
    }
}
